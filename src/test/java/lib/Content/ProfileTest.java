package lib.Content;

import Client.Client;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProfileTest {



    private static Profile testProfile;

    @org.junit.Before
    public void setUp() throws Exception {
        testProfile = new Profile();
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @Test
    public void testStatus() {
        assertEquals(testProfile.getStatus("Online"), "Online");
    }

    @Test
    public void testVorname() {
        assertNull(testProfile.getVorname());
        testProfile.setVorname("Hans");
        assertEquals(testProfile.getVorname(), "Hans");
    }

    @Test
    public void testNachname() {
        assertNull(testProfile.getNachname());
        testProfile.setNachname("Wurst");
        assertEquals(testProfile.getNachname(), "Wurst");
    }

    @Test
    public void testeMail() {
        try {
            testProfile.seteMail("DiesIstEineEmail");
            fail("Correct E-Mail's are accepted!");
        } catch (Exception e) {
        }
        try {
            testProfile.seteMail("Florian.Dobisch@googlemail.com");
        } catch (Exception e) {
            e.printStackTrace();
            fail("False E-Mail's are rejected!");
        }
    }

    @Test
    public void testAlter() throws Exception {
        assertEquals(testProfile.getAlter(), 0);
        try {
            testProfile.setAlter(-3);
            fail();
        } catch (Exception e) {
        }
        testProfile.setAlter(12);
        assertEquals(testProfile.getAlter(), 12);
    }
}