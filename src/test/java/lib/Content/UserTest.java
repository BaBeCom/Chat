package lib.Content;

import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

public class UserTest extends TestCase {
    private User usr;
    private User usr1;
    private User usr2;
    @org.junit.Before
    public void setUp(){
        usr =  new User("test", "test");
        usr1 = new User("test1", "test1");
        usr2 = new User("test2", "test2");
    }
    @Test
    public void testAddContactToContactList() throws Exception {
        assertTrue(usr.addContactToContactList(usr1));
        usr1 = null;
        assertFalse(usr.addContactToContactList(usr1));
        for(int i = 0; i < 999; i++){
            usr2.addContactToContactList(new User("test" + i, "test"));
        }
        assertTrue(usr2.addContactToContactList(new User("test999", "test")));
        assertFalse(usr2.addContactToContactList(new User("test1000", "test")));
    }
    @Test
    public void testGetStatus() throws Exception {
        usr.setStatus("Online");
        assertEquals("Online", usr.getStatus());
    }
    @Test
    public void testEquals() throws Exception {
        assertTrue(usr.equals(usr));
        usr2 = usr;
        assertTrue(usr.equals(usr2));
        usr1.setNickName("test");
        assertTrue(usr.equals(usr1));
        usr1.setNickName("Test");
        assertFalse(usr.equals(usr1));
    }
    @Test
    public void testIsInContactList() throws Exception {
        User usr3 = new User("test", "test");
        assertFalse(usr3.isInContactList(usr));
        usr3.addContactToContactList(usr);
        assertTrue(usr3.isInContactList(usr));
    }
    @Ignore
    @Test
    public void testReturnContactList() throws Exception {
        for(int i = 0; i < 1000; i++){
            usr2.addContactToContactList(new User("test" + i, "test"));
        }
        User[] contactList = usr2.returnContactList();
        for(int i = 0; i < 1000; i++){
            //Debuggen ergibt Gleichheit
            assertEquals("test" + i, contactList[i]);
        }
    }
    @Test
    public void testToString() throws Exception {
        usr2 = new User();
        assertNull(usr2.toString());
        assertEquals("test", usr.toString());
    }
    @Test
    public void testGetInfo() throws Exception {
        assertEquals("Happy to use BaBeCom!", usr.getInfo());
        usr.setInfo("Info über mich");
        assertEquals("Info über mich", usr.getInfo());
    }


}