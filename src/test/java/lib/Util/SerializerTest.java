package lib.Util;

import lib.Content.Message;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class SerializerTest {
    private static final String jsonStringOfTestMessage = "{\"localID\":4321,\"serverID\":\"ServerIDHashExample\",\"text\":\"Dies ist eine Testnachricht\",\"AuthorNickName\":\"Hans\",\"date\":\"Jan 18, 1970 2:01:28 PM\",\"chatmessageid\":1234,\"numInChat\":123}";
    private static Message testMessageObject;

    @org.junit.Before
    public void setUp() {
        testMessageObject = new Message();
        testMessageObject.setAuthorNickName("Hans");
        testMessageObject.setNumInChat(123l);
        testMessageObject.setChatmessageid(1234l);
        testMessageObject.setDate(new Date(1515688902l));
        testMessageObject.setText("Dies ist eine Testnachricht");
        testMessageObject.setLocalID(4321l);
        testMessageObject.setServerID("ServerIDHashExample");
    }

    @Test
    public void testMessageToJson() {
        assertEquals(testMessageObject.getAuthorNickName(), "Hans");
        assertEquals(testMessageObject.getNumInChat(), (Long) 123l);
        assertEquals(testMessageObject.getChatmessageid(), (Long) 1234l);
        assertEquals(testMessageObject.getDate(), new Date(1515688902l));
        assertEquals(testMessageObject.getText(), "Dies ist eine Testnachricht");
        assertEquals(testMessageObject.getLocalID(), (Long) 4321l);
        assertEquals(testMessageObject.getServerID(), "ServerIDHashExample");
//        assertEquals(Serializer.MessageToJson(testMessageObject), jsonStringOfTestMessage);
    }

    @Test
    public void testMessageFromJson() {
        Message convertedTestObject = Serializer.MessageFromJson(jsonStringOfTestMessage);
        assertEquals(convertedTestObject.getAuthorNickName(), "Hans");
        assertEquals(convertedTestObject.getNumInChat(), (Long) 123l);
        assertEquals(convertedTestObject.getChatmessageid(), (Long) 1234l);
        assertEquals(convertedTestObject.getText(), "Dies ist eine Testnachricht");
        assertEquals(convertedTestObject.getLocalID(), (Long) 4321l);
        assertEquals(convertedTestObject.getServerID(), "ServerIDHashExample");
    }
}