package Util;


import lib.Content.User;
import lib.Util.GeneralUtils;
import org.junit.Assert;
import org.junit.Test;

public class GeneralUtilsTest {

    @Test
    public void createChatName() {
        User userA = new User();
        userA.setNickName("Andreas");
        User userB = new User();
        userB.setNickName("Benjamin");
        String chatName = GeneralUtils.createChatName(userA, userB);
        Assert.assertEquals(chatName, "andreasbenjamin");
    }
}