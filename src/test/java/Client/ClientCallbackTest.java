package Client;


import junit.framework.TestCase;
import lib.Content.Message;
import lib.Content.User;

public class ClientCallbackTest extends TestCase {
    User usr;
    User usr2;
    Client client;
    Message msg;
    public void setUp(){
        usr = new User("test", "test");
        usr2 = new User("test1", "test1");
        client = new Client(usr);

    }
    public void testMessageArrived() throws Exception {
        assertNull(client.getNextMessage());
        msg = new Message();
        msg.setText("Test Message");
        msg.setAuthorNickName(usr.getNickName());
        client.sendMessage(msg);
        testMessageArrived();
        assertTrue(client.hasNextMessage());
    }

}