package Client;

import com.mongodb.gridfs.CLI;
import junit.framework.TestCase;
import lib.Content.Message;
import lib.Content.User;
import org.junit.Ignore;
import org.junit.Test;

import java.net.MalformedURLException;

public class ClientTest extends TestCase {
    User usr;
    User usr2;
    Client client;
    Message msg;
    @org.junit.Before
    public void setUp(){
        usr = new User("test", "test");
        client =  new Client(usr);
    }
    /*
      Testet auch Methode addQueueToQueue(Message msg)
     */
   @Test
    public void testGetNextMessage() throws Exception {
        assertNull(client.getNextMessage());
        for(int i = 0; i<10; i++) {
            msg = new Message();
            msg.setText("Test Message " + i);
            msg.setAuthorNickName(usr.getNickName());
            client.sendMessage(msg);
            Message nextMessage = client.getNextMessage();
            assertEquals(client.getNextMessage(), nextMessage);
        }

    }

    @Test
    private void testLoginClient(User pUser, String username, String pw) throws Exception {
        switch (username){
            case "testUser": assertEquals(pUser, Client.login(username, pw));
                break;
            case "": assertEquals(new User("Fehler! Benutzer existiert nicht."), Client.login(username, pw));
                break;
            case "testUser1": assertEquals(new User("Fehler! Benutzer existiert nicht."), Client.login(username, pw));
                break;
            case "testUser2": assertEquals(new User("Fehler! Benutzer existiert nicht."), Client.login(username, pw));
                break;
            case " ": assertEquals(new User("Fehler! Benutzer existiert nicht."), Client.login(username, pw));
                break;
            default:
                throw new Exception();
        }



    }

    @Test
    public void testRegister() throws MalformedURLException {
        String username = "testUser";
        String pw = "passwort123";
        ServerUtils.getServerWS().removeByNickName(username);
        User usr = new User(username, Client.sha256(pw));
        assertNull(Client.register(username, pw));
        try {
            testLoginClient(usr, username, pw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String emptusrname = "";
        String emptpw = "";
        User usr1 = new User(emptusrname, emptpw);
        assertEquals("Fehler! Benutzername darf keine Sonderzeichen enthalten oder leer sein!", Client.register(emptusrname, emptpw));
        try {
            testLoginClient(usr1, emptusrname, emptpw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String username1 = "testUser1";
        User usr2 = new User(username1, emptpw);
        assertEquals("Fehler! Passwort darf keine Leerzeichen enthalten oder leer sein", Client.register(username1, emptpw));
        try {
            testLoginClient(usr2, username1, emptpw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String username2 = " ";
        User usr3 = new User(username2, pw);
        assertEquals("Fehler! Benutzername darf keine Sonderzeichen enthalten oder leer sein!", Client.register(username2, pw));
        try {
            testLoginClient(usr3, username2, pw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String username3 = "testUser2";
        String whiteSpacepw = " ";
        User usr4 = new User(username3, whiteSpacepw);
        assertEquals("Fehler! Passwort darf keine Leerzeichen enthalten oder leer sein", Client.register(username3, whiteSpacepw));
        try {
            testLoginClient(usr4, username3, whiteSpacepw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Test ob erkannt wird, dass Nutzer schon vorhanden ist
        ServerUtils.getServerWS().removeByNickName("abc");
        assertNull(Client.register("abc", "passwort"));
        assertNotNull(Client.register("abc", "passwort"));
    }

    //Client ist bereits User "test" zugewiesen
    @Test
    public void testGetLoggedInUser(){
        client = new Client(usr);
        assertEquals(usr, client.getLoggedIn());
    }

    @Test
    public void testCheckForWhitespace(){
        String str = "";
        String str2 = " ";
        String str3 = "a b";
        String str4 = " ab";
        String str5 = "ab ";
        String str6 = "ab";
        assertFalse(client.checkForWhitespace(str));
        assertTrue(client.checkForWhitespace(str2));
        assertTrue(client.checkForWhitespace(str3));
        assertTrue(client.checkForWhitespace(str4));
        assertTrue(client.checkForWhitespace(str5));
        assertFalse(client.checkForWhitespace(str6));
    }

    @Test
    public void testCheckForSpecialCharacters(){
        String str = "";
        String str1 = "*";
        String str2 = "!";
        String str3 = "abc";
        assertTrue(client.checkForSpecialCharacter(str));
        assertTrue(client.checkForSpecialCharacter(str1));
        assertTrue(client.checkForSpecialCharacter(str2));
        assertFalse(client.checkForSpecialCharacter(str3));

    }

    @Test
    public void testSearchForUser(){
        assertNotNull(Client.searchForUser("notExistingUser").getErrormsg());
        Client.register("abc", "passwort");
        assertNull(Client.searchForUser("abc").getErrormsg());
        assertNotNull((Client.searchForUser("").getErrormsg()));
    }

    @Test
    public void testSubscribeOnTopic(){
        String topic = "/Chatrooms/1/Send";
        assertTrue(client.subscribeOnTopic(topic));
        String topic2 = "";
        assertFalse(client.subscribeOnTopic(topic2));
    }



}