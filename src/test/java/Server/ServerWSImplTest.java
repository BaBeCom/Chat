package Server;

import Client.Client;
import Client.ServerUtils;
import lib.Content.User;
import org.junit.Test;

import javax.xml.ws.Endpoint;
import java.net.MalformedURLException;

import static org.junit.Assert.assertEquals;

public class ServerWSImplTest {
    String username = "test5";
    String password = "test5";
    String empt = "";
    User u;
    User v;

    @org.junit.BeforeClass
    public static void SetupServerStuff() {
        Endpoint.publish("http://127.0.0.1:6666/ws/serverws", new ServerWSImpl()); // Für die Clients
        Endpoint.publish("http://127.0.0.1:6666/ws/mongows", new MongoWSImpl()); // Für sich selbst
    }

    @org.junit.Before
    public void SetUp() {
        u = new User(username, Client.sha256(password));
    }

    @Test
    public void testCreateUser() throws MalformedURLException {
        //testet, ob user in der DB erstellt werden kann
        v = ServerUtils.getServerWS().createUser(username, password);
        assertEquals(u, v);

        //testet, ob user erstellt werden kann, welcher bereits in der DB existiert
        u = new User("Fehler! Nickname \"" + username + "\" bereits vergeben");
        v = ServerUtils.getServerWS().createUser(username, password);
        assertEquals(u, v);
    }

    @Test
    public void testLoginUser() throws MalformedURLException {
        //Überprüft login mit bestehendem Nutzer und korrekten Daten
        v = ServerUtils.getServerWS().createUser(username, password);
        assertEquals(u, ServerUtils.getServerWS().login(username, password));

        //Überprüft Login mit falschem Passwort
        u = new User("Fehler! Passwort falsch.");
        assertEquals(u, ServerUtils.getServerWS().login(username, password + " "));

        //Überprüft Login mit nicht existemtem Nutzer
        u = new User("Fehler! Benutzer existiert nicht.");
        assertEquals(u, ServerUtils.getServerWS().login(username + "1", password));
    }

    @Test
    public void testSearchForUser() throws MalformedURLException {
        //Sucht nach einem existierenden Benutzer
        v = ServerUtils.getServerWS().createUser(username, password);
        v = ServerUtils.getServerWS().searchForUser(username);
        assertEquals(u, v);

        u = new User("Fehler! Benutzer existiert nicht.");
        assertEquals(u, ServerUtils.getServerWS().searchForUser(username + "1"));
    }

    @org.junit.After
    public void tearDown() throws MalformedURLException {
        ServerUtils.getServerWS().removeByNickName(username);
    }
}