package lib.Util;

import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.net.MalformedURLException;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ServerWS {

    @WebMethod
    public User createUser(String nickname, String password) throws MalformedURLException;

    @WebMethod
    public User login(String nickname, String password) throws MalformedURLException;

    @WebMethod
    Chat createChat(String name);

    @WebMethod
    Message[] getMessagesFrom(Long chatMessageid, Long numInChat) throws MalformedURLException;

    @WebMethod
    User searchForUser(String username) throws MalformedURLException;

    @WebMethod
    boolean removeByNickName(String nickname);

    @WebMethod
    void saveUser(User loggedIn) throws MalformedURLException;

    @WebMethod
    Long getMaxNumInChat(Long chatMessageID) throws MalformedURLException;
}
