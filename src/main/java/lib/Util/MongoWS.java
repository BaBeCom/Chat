package lib.Util;

import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MongoWS {
    @WebMethod
    public Message getMessageBevore(Message msg);

    @WebMethod
    public void saveMessage(Message msg);

    @WebMethod
    User[] getContactListFrom(User user);

    @WebMethod
    Message[] getMessagesFrom(Long chatMessageid, Long numInChat);

    @WebMethod
    void saveUser(User user);

    @WebMethod
    public User searchForUser(String nickname);

    @WebMethod
    boolean removeByNickName(String nickname);

    @WebMethod
    Chat getChatByChatName(String name);

    @WebMethod
    Long getMaxChatMessageID(Long chatMessageID);
}
