package lib.Util;

import com.google.gson.Gson;
import lib.Content.ChatRequest;
import lib.Content.Message;
import lib.Content.User;

/**
 * Helfer zum Serialisieren von Objekten um sie als Json über MQTT versenden zu können.
 */
public class Serializer {
    public static String MessageToJson(Message input) {
        Gson gson = new Gson();
        String out = gson.toJson((input));
        return out;
    }

    public static Message MessageFromJson(String json) {
        return new Gson().fromJson((json), Message.class);
    }

    public static ChatRequest ChatRequestFromJson(String message) {
        return new Gson().fromJson(message,ChatRequest.class);
    }
    public static String ChatRequestToJson(ChatRequest input) {
        Gson gson = new Gson();
        String out = gson.toJson((input));
        return out;
    }

    public static String UserToString(User user) {
        return new Gson().toJson(user);
    }
}