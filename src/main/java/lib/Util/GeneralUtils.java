package lib.Util;

import lib.Content.User;

public class GeneralUtils {
    public static String createChatName(User userA, User userB){
        String nickNameA = userA.getNickName().toLowerCase();
        String nickNameB = userB.getNickName().toLowerCase();
        if (nickNameA.compareTo(nickNameB)<0)
            return nickNameA+nickNameB;
        else
            return nickNameB+nickNameA;
    }
}
