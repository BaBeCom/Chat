package lib.Util;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ServerUtils {
    private static ServerWS sws = null;

    public static ServerWS getServerWS() {
        try {
            if (sws == null) {
                URL url = new URL("http://" + Server.Server.serverIP + ":6666/ws/serverws?wsdl");
                QName qname = new QName("http://Server/", "ServerWSImplService");
                Service service = Service.create(url, qname);
                sws = service.getPort(ServerWS.class);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return sws;
    }
}
