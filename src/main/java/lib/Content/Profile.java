package lib.Content;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(name = "Profile", uniqueConstraints={@UniqueConstraint(columnNames={"localID"})})
public class Profile {
    private static final String RFC5322_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

    public final static  String ONLINE = "Online";
    public final static  String ABWESEND = "Abwesend";
    public final static  String BESCHÄFTIGT = "Beschäftigt";
    public final static  String OFFLINE = "Offline";

    @Column(name="status")
    private String status;

    @Column(name="firstName")
    private String vorname;

    @Column(name="lastName")
    private String nachname;

    @Column(name="eMail")
    private String eMail;

    @Column(name="age")
    private int alter;


    public Profile() {
        this.status = ONLINE;
    }

    public void setStatus(String status) {
        this.status = getStatus(status);
    }

    public String getStatus (String status) {
        if(status == "Online") {
            this.status = ONLINE;
            return "Online";
        } else if (status == "Abwesend") {
            this.status = ABWESEND;
            return "Abwesend";
        } else if (status == "Beschäftigt") {
            this.status = BESCHÄFTIGT;
            return "Beschäftigt";
        } else if (status == "Offline") {
            this.status = OFFLINE;
            return "Offline";
        } else {
            return "Error !";
        }
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) throws Exception {
        if (!eMail.toLowerCase().matches(RFC5322_REGEX))
            throw new Exception("Not a valid E-Mail!");
        this.eMail = eMail;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) throws Exception {
        if (alter < 0)
            throw new Exception("Negative age used!");
        this.alter = alter;
    }
}
