package lib.Content;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import lib.Util.Serializer;

import javax.imageio.ImageIO;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Datenobjekt für eine Standart Nachricht.
 * Annotiert für Hibernate um Message-Objekte in Lokalen Datenbank Cachen zu können.
 */
@Entity
@Table(name = "Message", uniqueConstraints={@UniqueConstraint(columnNames={"localID"})})
public class Message implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "localID", nullable = false, unique = true, length = 11)
    private Long localID;

    @Column(name = "serverID", nullable = true, unique = true, length = 30)
    private String serverID;

    @Column(name = "text")
    private String text = "";

    @Column(name = "AuthorNickName")
    private String AuthorNickName;

    @Column(name = "date")
    private Date date;

    @Column(name = "chatmessageid")
    private Long chatmessageid;

    @Column(name = "numInChat")
    private Long numInChat;

    @Column(name = "bytes")
    private byte[] bytes;


    public Message(){}

    public Message(String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Long getLocalID() {
        return localID;
    }

    public void setLocalID(Long localID) {
        this.localID = localID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getChatmessageid() {
        return chatmessageid;
    }

    public void setChatmessageid(Long chatmessageid) {
        this.chatmessageid = chatmessageid;
    }

    public String getAuthorNickName() {
        return AuthorNickName;
    }

    public void setAuthorNickName(String authorNickName) {
        AuthorNickName = authorNickName;
    }

    @Override
    public String toString() {
        return Serializer.MessageToJson(this);
    }

    public Long getNumInChat() {
        return numInChat;
    }

    public void setNumInChat(Long numInChat) {
        this.numInChat = numInChat;
    }

    public String getServerID() {
        return serverID;
    }

    public void setServerID(String serverID) {
        this.serverID = serverID;
    }


    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }


    public byte[] imageToByte(Image image) {
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream s = new ByteArrayOutputStream();
        try {
            ImageIO.write(bImage, "jpg", s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        bytes = s.toByteArray();
        return bytes;
    }

    public Image byteToImage() {
        Image image = new Image("/img/icon.png");
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            BufferedImage read = ImageIO.read(bis);
            image = SwingFXUtils.toFXImage(read, null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return image;
    }
}
