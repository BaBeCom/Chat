package lib.Content;


import javax.persistence.*;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"localID"})})
public class Chat {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long localID;

    @Column
    private Long chatmessageid;

    @Column
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getLocalID() {
        return localID;
    }

    public void setLocalID(Long localID) {
        this.localID = localID;
    }

    public Long getChatmessageid() {
        return chatmessageid;
    }

    public void setChatmessageid(Long chatmessageid) {
        this.chatmessageid = chatmessageid;
    }
}
