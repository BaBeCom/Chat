package lib.Content;

import lib.Util.Serializer;

public class ChatRequest {
    private Chat chat;
    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    @Override
    public String toString() {
        return Serializer.ChatRequestToJson(this);
    }
}
