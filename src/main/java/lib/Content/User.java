package lib.Content;


import javax.persistence.*;

/**
 * Datenobjekt für einen User.
 */
@Entity
@Table(name = "User", uniqueConstraints = {@UniqueConstraint(columnNames = {"_id"})})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id", nullable = false, unique = true, length = 11)
    private String _id;

    @Column(name = "err", length = 100)
    private String errormsg;

    @Column(name = "password", nullable = false, length = 20)
    private String password;

    @Column(name = "nickName", nullable = false, unique = true, length = 20)
    private String nickName;

    private int contactListIndex = 0;

    @Column(name = "info")
    private String info;

    public void setfriendslist(User[] contactList) {
        this.contactList = contactList;
        this.contactListsize=contactList.length;
        for (int i = 0; i < contactList.length; i++) {
            if(contactList[i]==null) {
                contactListIndex = i;
                break;
            }
        }
    }

    private int contactListsize = 1000;

    public User[] getfriendsList() {
        return contactList;
    }

    //maximum of 1000 contacts
    private User[] contactList = new User[1000];

    public User() {
    }

    public User(String name, String password) {
        this.password = password;
        this.nickName = name;
        this.errormsg = null;
        this.info = "Happy to use BaBeCom!";
    }

    /**
     * Wird zur Fehlerbehandlung benutzt, um serverseitige Fehler zu unterscheiden
     * @param errorMsg Fehlernachricht, um zu beschreiben, welcher Fehler aufgetreten ist
     */
    public User(String errorMsg) {
        this.errormsg = errorMsg;
        this.password = "";
        this.nickName = "";
    }

    @Column(name = "status")
    private String status;

    //getter

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Wird zur Fehlerbehandlung benutzt, um serverseitige Fehler zu unterscheiden
     * @return Gibt an, welcher Fehler serverseitig aufgetreten ist
     */
    public String getErrormsg() { return errormsg; }

    public void setErrormsg(String errormsg) { this.errormsg = errormsg; }

    public boolean addContactToContactList(User usr) {
        if (usr != null) {
            if (contactListIndex >= contactListsize) {
                return false;
            }
            contactList[contactListIndex] = usr;
            contactListIndex++;
            return true;
        }
        return false;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Überprüft, ob zwei User identisch sind --> nickname ist einzigartig
     * Methode wird in der ObservableList aufgerufen
     * @param obj Vergleichsuserobjekt
     * @return Ist user identisch mit dem übergebenen Objekt?
     */
    @Override
    public boolean equals(Object obj) {
        if(obj==null) return false;
        User usr = User.class.cast(obj);
        if (this.getNickName().equals(usr.getNickName())) {
            return true;
        }
        return false;
    }

    public boolean isInContactList(User usr){
        for(int i=0; i<contactListIndex; i++){
            if(contactList[i].equals(usr)){
                return true;
            }
        }
        return false;
    }

    public User[] returnContactList(){
        return contactList;
    }

    @Override
    public String toString(){
        if(nickName != null) return nickName;
        return null;
    }

    public String getInfo() {
            return info;

    }

    public void setInfo(String info) {
        this.info = info;
    }
}
