package Server;

import lib.Content.Message;
import lib.Util.Serializer;
import org.eclipse.paho.client.mqttv3.*;

import java.net.MalformedURLException;
import java.util.Date;

public class ServerCallback implements MqttCallback {

    private MqttClient client;

    public ServerCallback(MqttClient client) {
        super();
        this.client = client;
    }

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        String message = mqttMessage.toString();
        System.out.print(message);
        if (topic.matches("/Chatrooms/[a-zA-Z0-9]+/Receive")) {
            Message msg = Serializer.MessageFromJson(message);
            msg.setDate(new Date());
            if (msg.getNumInChat() == null) {
                setNumInChat(msg);
            }
            System.out.println("Datum Gesetzt: " + msg);
            MongoUtils.getMongoWS().saveMessage(msg);
            SendMessage(msg);
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    /**
     * Bestimmt die Chronologische Nummer der Nachricht im Chatverlauf.
     *
     * @param msg Message-Objekt welches "einsortiert" werden soll
     * @return msg ergänzt um die numInChat.
     */
    private Message setNumInChat(Message msg) {
        try {
            Message bevore = MongoUtils.getMongoWS().getMessageBevore(msg);
            if (bevore == null)
                msg.setNumInChat(1l);
            else
                msg.setNumInChat(bevore.getNumInChat() + 1l);
            MongoUtils.getMongoWS().saveMessage(msg);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return msg;
    }

    /**
     * Sendet Nachricht raus an alle Clients.
     *
     * @param msg Die zu Sendende Nachricht
     */
    public void SendMessage(Message msg) {
        try {
            String topic = "/Chatrooms/" + msg.getChatmessageid() + "/Send";
            System.out.println("Send Message: ( Topic: "+topic+", Message:" + msg + " )");
            client.publish(topic, Serializer.MessageToJson(msg).getBytes(), 2, false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
