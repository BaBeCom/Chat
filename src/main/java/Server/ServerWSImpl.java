package Server;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.User;
import lib.Util.ServerWS;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import javax.jws.WebService;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@WebService(endpointInterface = "lib.Util.ServerWS")
public class ServerWSImpl implements ServerWS {
    /**
     * Falls angegebener nickname noch nicht als User existiert, wird ein neuer User in der DB erstellt
     * @param nickname Nutzername des zu erstellenden Users
     * @param password Passwort des zu erstellenden Users
     * @return Entweder gültiger Nutzer oder Erroruser, falls Nutzer bereits vorhanden
     * @throws MalformedURLException
     */
    @Override
    public User createUser(String nickname, String password) throws MalformedURLException {
        User usr = new User(nickname, password);

        if (!(MongoUtils.getMongoWS().searchForUser(nickname).getErrormsg() == null)) {
            MongoUtils.getUserCollection().insertOne(usr);
            System.out.println("Benutzer " + nickname + " wurde erstellt.");
        } else {
            usr = new User("Fehler! Nickname \"" + nickname + "\" bereits vergeben");
        }
        return usr;
    }

    /**
     *
     * @param user
     * @param password
     * @return Entweder gültiger Nutzer oder Erroruser, falls Nutzer nicht existiert oder das Passwort falsch ist (unterscheidung über Errormsg)
     * @throws MalformedURLException
     */
    @Override
    public User login(String user, String password) throws MalformedURLException {
        User usr = MongoUtils.getMongoWS().searchForUser(user);

        if (usr.getErrormsg() == null) {
            if (!usr.getPassword().equals(password)) {
                usr = new User("Fehler! Passwort falsch.");
            }
        } else {
            usr = new User("Fehler! Benutzer existiert nicht.");
        }
        return usr;
    }

    /**
     *
     * @param nickname Nickname des zu löschenden Nutzers
     * @return Gibt true zurück, falls Löschen erfolgreich war
     */
    @Override
    public boolean removeByNickName(String nickname){
        Bson condition = new Document("$eq",nickname);
        Bson filter = new Document("nickName",condition);
        try{
            if(searchForUser(nickname).getErrormsg() == null){
                MongoUtils.getUserCollection().deleteOne(filter);
                return true;
            }
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Chat createChat(String name) {
        MongoCollection collection = MongoUtils.getChatCollection();
        Chat chat = null;
        try {
            chat = MongoUtils.getMongoWS().getChatByChatName(name);
            if (chat.getChatmessageid()!=null && chat.getName()!= null)
                return chat;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        chat.setName(name);
        FindIterable findIterable = collection.find().sort(Document.parse("{\"chatmessageid\":-1}")).limit(1);
        Chat c = null;
        try {
            c = (Chat) findIterable.first();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (c == null) {
            chat.setChatmessageid(1l);
        } else {
            chat.setChatmessageid(c.getChatmessageid() + 1l);
        }
        collection.insertOne(chat);
        return chat;
    }

    @Override
    public Message[] getMessagesFrom(Long chatMessageid, Long numInChat) throws MalformedURLException {
        return MongoUtils.getMongoWS().getMessagesFrom(chatMessageid, numInChat);
    }

    @Override
    public User searchForUser(String username) throws MalformedURLException {
        return MongoUtils.getMongoWS().searchForUser(username);
    }

    @Override
    public void saveUser(User user) throws MalformedURLException {
        MongoUtils.getMongoWS().saveUser(user);
    }

    @Override
    public Long getMaxNumInChat(Long chatMessageID) throws MalformedURLException {
        return MongoUtils.getMongoWS().getMaxChatMessageID(chatMessageID);
    }
}