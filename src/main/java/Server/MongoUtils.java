package Server;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.User;
import lib.Util.MongoWS;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * Helfer für Mongodb.
 * Ist AUSSCHLIESSLICH für den Server gedacht.
 */
public class MongoUtils {
    private static MongoWS sws = null;
    private static MongoCollection messageCollection = null;
    private static MongoCollection userCollection = null;
    private static MongoCollection chatCollection = null;
    private static CodecRegistry pojoCodecRegistry;
    private static MongoClient mongoClient;

    public static MongoClient getMongoClient() {
        if (pojoCodecRegistry == null) {
            pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(), fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            mongoClient = new MongoClient("chat.dobisch.online", MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
        }
        return mongoClient;
    }


    public static MongoCollection getMessageCollection() {
        if (messageCollection == null) {
            MongoDatabase messagesDB = getMongoClient().getDatabase("Messages");
            messagesDB.withCodecRegistry(pojoCodecRegistry);
            messageCollection = messagesDB.getCollection("Message", Message.class);
        }
        return messageCollection;
    }

    public static MongoCollection getChatCollection() {
        if (chatCollection == null) {
            MongoDatabase messagesDB = getMongoClient().getDatabase("Chats");
            messagesDB.withCodecRegistry(pojoCodecRegistry);
            chatCollection = messagesDB.getCollection("Chat", Chat.class);
        }
        return chatCollection;
    }

    public static MongoCollection getUserCollection() {
        if (userCollection == null) {
            MongoDatabase userDB = getMongoClient().getDatabase("User");
            userDB.withCodecRegistry(pojoCodecRegistry);
            userCollection = userDB.getCollection("User", User.class);
        }
        return userCollection;
    }

    /**
     * Sorgt dafür dass nur eine verbindung pro Server instanz zum MongoDB WebService aufgebaut wird.
     *
     * @return Der MongoDB WebService
     * @throws MalformedURLException
     */
    public static MongoWS getMongoWS() throws MalformedURLException {
        if (sws == null) {
            URL url = new URL("http://" + Server.serverIP + ":6666/ws/mongows?wsdl");
            QName qname = new QName("http://Server/", "MongoWSImplService");
            Service service = Service.create(url, qname);
            sws = service.getPort(MongoWS.class);
        }
        return sws;
    }
}
