package Server;

import com.mongodb.client.FindIterable;
import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.User;
import lib.Util.MongoWS;
import lib.Util.Serializer;
import org.bson.Document;
import org.bson.conversions.Bson;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "lib.Util.MongoWS")
public class MongoWSImpl implements MongoWS {


    /**
     * Liefert die Nachricht vor der Angegebenen.
     *
     * @param msg
     * @return Die vorherige Nachricht, wenn es keine gibt null
     */
    @Override
    public Message getMessageBevore(Message msg) {
        FindIterable findIterable = MongoUtils.getMessageCollection().find(Document.parse("{chatmessageid:NumberLong(" + msg.getChatmessageid() + ")}"), Message.class).sort(Document.parse("{\"numInChat\":-1}")).limit(1);
        Message ret = null;
        try {
            ret = (Message) findIterable.first();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ret == null) { // TODO: Andere lösung finden was man macht wenn es die erste nachricht in einem neuen chat ist.
            ret = new Message();
            ret.setNumInChat(0l);
        }
        return ret;
    }

    /**
     * Speichert eine Nachricht in MongoDB
     *
     * @param msg
     */
    @Override
    public void saveMessage(Message msg) {
        MongoUtils.getMessageCollection().insertOne(msg);
    }

    @Override
    public User[] getContactListFrom(User user) {
        return new User[0];
    }

    @Override
    public Message[] getMessagesFrom(Long chatMessageid, Long numInChat) {
        FindIterable<Message> resultSet = MongoUtils.getMessageCollection().find(Document.parse("{\"chatmessageid\":NumberLong(" + chatMessageid + "), \"numinchat\":{ $gte : NumberLong(" + numInChat + ")}}"));
        List<Message> msgList = new ArrayList<>();
        for (Message msg : resultSet) {
            msgList.add(msg);
        }
        Message ret[] = new Message[msgList.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = msgList.get(i);
        }
        return ret;
    }

    @Override
    public void saveUser(User user) {
        System.out.println("MongoWS, SaveUser: " + user);
        for (User usr :
                user.getfriendsList()) {
            if (usr != null)
                System.out.println("\t" + usr);
        }
        User temp = searchForUser(user.getNickName());
        temp.setStatus(user.getStatus());
        temp.setfriendslist(user.getfriendsList());
        if (user.getPassword() != null)
            temp.setPassword(user.getPassword());
        if (user.getInfo() != null)
            temp.setInfo(user.getInfo());
        MongoUtils.getUserCollection().findOneAndReplace(Document.parse("{ \"nickName\" : \"" + user.getNickName() + "\" }"), Document.parse(Serializer.UserToString(temp)));
    }

    @Override
    public User searchForUser(String nickname) {
        Bson condition = new Document("$eq", nickname);
        Bson filter = new Document("nickName", condition);
        User usr;

        if (MongoUtils.getUserCollection().count(filter) != 0) {
            usr = (User) MongoUtils.getUserCollection().find(filter).first();
        } else {
            usr = new User("Fehler! User mit dem Namen " + nickname + " wurde nicht gefunden.");
        }
        return usr;
    }

    @Override
    public boolean removeByNickName(String nickname) {
        Bson condition = new Document("$eq", nickname);
        Bson filter = new Document("nickName", condition);
        if (searchForUser(nickname).getErrormsg().equals(null)) {
            MongoUtils.getUserCollection().deleteOne(filter);
            return true;
        } else return false;
    }

    @Override
    public Chat getChatByChatName(String name) {
        Chat chat = (Chat) MongoUtils.getChatCollection().find(Document.parse("{ \"name\" : \"" + name + "\"}")).sort(Document.parse("{ \"chatmessageid\" : -1 }")).first();
        if (chat != null)
            return chat;
        chat = new Chat();
        return chat;
    }

    @Override
    public Long getMaxChatMessageID(Long chatMessageID) {
        return (Long) MongoUtils.getMessageCollection().find(Document.parse("{ \"chatmessageid\" : \"" + chatMessageID + "\"}")).sort(Document.parse("{ \"numInChat\": -1 }")).first();
    }
}
