package Server;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import javax.xml.ws.Endpoint;
import java.util.Scanner;

/**
 * Server Hauptklasse
 */
public class Server {
    // wird von MongoUtils, ServerUtils und Server genutzt und dient zum Einstellen der IP des zu anzusprechenden Servers
    public static String serverIP="192.168.178.28";
    private MqttClient client = null;

    /**
     * Server Stellt sich selbst Mongo-WebService und den Clients den Server-WebService zur verfügung.
     * Verbindet sich mit MQTT-Broker, und horcht auf Messages auf allen Chat Räumen.
     */
    public Server() {
        Endpoint.publish("http://" + serverIP + ":6666/ws/serverws", new ServerWSImpl()); // Für die Clients
        Endpoint.publish("http://" + serverIP + ":6666/ws/mongows", new MongoWSImpl()); // Für sich selbst

        try {
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            client = new MqttClient("tcp://chat.dobisch.online:32768", "Server" + System.currentTimeMillis());
            client.setCallback(new ServerCallback(client));
            client.connect(connOpts);
            client.subscribe("/Chatrooms/+/Receive");
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Server erfolgreich Gestartet!");
        System.out.println("exit schreiben zum beenden");
        Scanner scan = new Scanner(System.in);
        boolean beenden = false;
        while (!beenden) {
            if (scan.next().equals("exit"))
                beenden = true;
        }
        System.exit(0);
    }

    public static void main(String[] args) {
        new Server();
    }
}
