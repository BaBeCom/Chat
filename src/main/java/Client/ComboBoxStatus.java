package Client;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;

public class ComboBoxStatus extends ComboBox {

    GUIfx gui;
    public ComboBoxStatus(GUIfx gui, HBox borderImageViewProfile) {
        this.gui = gui;
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Online",
                        "Abwesend",
                        "Beschäftigt",
                        "Offline"
                );
        this.getItems().addAll(options);
        this.setValue("Online");
        gui.getClient().getLoggedInUser().setStatus("Online");
        try {
            ServerUtils.getServerWS().saveUser(gui.getClient().getLoggedInUser());
        }catch(Exception e){
            e.printStackTrace();
        }

        valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                gui.getClient().getLoggedInUser().setStatus((String)newValue);
                try {
                    ServerUtils.getServerWS().saveUser(gui.getClient().getLoggedInUser());
                }catch(Exception e){
                    e.printStackTrace();
                }
                if (newValue == "Online") {
                    //client.getLoggedInUser().setStatus("Online");
                    borderImageViewProfile.setStyle("-fx-border-color: green;"
                            + "-fx-border-width: 2;"
                            + "-fx-border-style: solid;");
                } else if (newValue == "Abwesend") {
                    //client.getLoggedInUser().setStatus("Abwesend");
                    borderImageViewProfile.setStyle("-fx-border-color: goldenrod;"
                            + "-fx-border-width: 2;"
                            + "-fx-border-style: solid;");
                } else if (newValue == "Beschäftigt") {
                    //client.getLoggedInUser().setStatus("Beschäftigt");
                    borderImageViewProfile.setStyle("-fx-border-color: red;"
                            + "-fx-border-width: 2;"
                            + "-fx-border-style: solid;");
                } else if (newValue == "Offline") {
                    //client.getLoggedInUser().setStatus("Offline");
                    borderImageViewProfile.setStyle("-fx-border-color: gray;"
                            + "-fx-border-width: 2;"
                            + "-fx-border-style: solid;");
                }
            }
        });
    }

}

