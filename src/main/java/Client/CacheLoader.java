package Client;

import lib.Content.Chat;
import lib.Content.Message;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;

public interface CacheLoader {
    public Collection<Message> getMessagesByChatID(Long chatmessageid);

    Collection<Message> getMessagesByChatIDUntil(Long ChatID, long UntilID);

    public List<Chat> getChats();

    void updadateChat(Long chatMessageid, Long numInChat);

    Long maxNumInChat(Long chatMessageID) throws MalformedURLException;
}
