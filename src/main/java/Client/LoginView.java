package Client;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lib.Content.User;

import java.util.Optional;



public class LoginView extends Application {
    GUIfx gui;
    private static final String RFC5322_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

    Stage primaryStage;
    String view;

    GridPane gp;
    GridPane gp2;

    Label lblNickname;
    TextField nickname;
    Label lblPassword;
    PasswordField password;
    Label lblNickname2;
    TextField nickname2;
    Label lblPassword2;
    PasswordField password2;
    Label lblpasswordWdh;
    TextField passwordWdh;
    Label mailLabel;
    TextField eMail;

    Button btnLog;
    Button btnReg;
    HBox btnRow;

    Button btnLog2;
    Button btnReg2;
    HBox btnRow2;

    public LoginView(GUIfx gui) {
        this.gui = gui;
        view = "login";
    }

    @Override
    public void start(Stage primStage) throws Exception {
        primaryStage=primStage;
        primaryStage.setResizable(false);
        primaryStage.setTitle("Login");
        primaryStage.getIcons().add(new Image("/img/babecom_icon.png"));

        gp = new GridPane();
        gp2 = new GridPane();

        buildLoginWindow();
        buildRegisterWindow();

        Scene log = new Scene(gp);
        Scene reg = new Scene(gp2);

        log.getStylesheets().add(GUIfx.class.getResource(
                "/css/statusBorderBackground.css").toExternalForm());
        reg.getStylesheets().add(GUIfx.class.getResource(
                "/css/statusBorderBackground.css").toExternalForm());

        primaryStage.setScene(log);
        primaryStage.show();
        primaryStage.show();

        //ermöglicht es, durch Drücken von Enter den Login auszuführen
        log.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode().equals(KeyCode.ENTER)&& view.equals("login")){
                    loginRoutine();
                }
            }
        });

        //ermöglicht es, durch Drücken von Enter die Registrierung durchzuführen
        reg.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode().equals(KeyCode.ENTER)&& view.equals("register")){
                    registerRoutine();
                }
            }
        });

        //routine für Login
        btnLog.setOnAction(event -> {
            if (view == "login") {
                loginRoutine();
            } else if (view == "register") {
                view = "login";
                primaryStage.hide();
                primStage.setTitle("Login");
                gp.add(btnRow, 1, 4, 2, 1);
                primaryStage.setScene(log);
                primaryStage.show();
            }
        });

        //routine für Registrierung
        btnReg.setOnAction(event -> {
            if (view == "login") {
                view = "register";
                primaryStage.hide();
                primaryStage.setTitle("Registrierung");
                gp2.add(btnRow, 1, 6, 2, 1);
                primaryStage.setScene(reg);
                primaryStage.show();
            } else {
                registerRoutine();
            }
        });

        //wird ausgeführt, wenn Login-Fenster geschlossen wird
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
    }

    private void loginRoutine(){
        if (nickname.getText().isEmpty() || password.getText().isEmpty()) {
            popUpMsg(msgType.ERR, "Bitte alle Felder ausfüllen!");
        } else {
            User loginReturn = Client.login(nickname.getText(), password.getText());
            if (loginReturn.getErrormsg() == null) {
                primaryStage.close();
                gui.startClientInit(loginReturn);
                gui.openChatWindow();
            } else {
                popUpMsg(msgType.ERR, loginReturn.getErrormsg());
            }
        }
    }

    private void registerRoutine(){
        if (!eMail.getText().toLowerCase().matches(RFC5322_REGEX)) {
            popUpMsg(msgType.ERR, "E-Mail Adresse ungültig!");
        } else if (!password2.getText().equals(passwordWdh.getText())) {
            popUpMsg(msgType.ERR, "Passwörter stimmen nicht überein!");
        } else {
            String registReturn = gui.getClient().register(nickname2.getText(), password2.getText());
            if (registReturn == null) {
                popUpMsg(msgType.REG, "Nutzer wurde erfolgreich erstellt");
                nickname2.clear();
                password2.clear();
                passwordWdh.clear();
                eMail.clear();
            } else {
                popUpMsg(msgType.ERR, registReturn);
            }
        }
    }

    private enum msgType {
        ERR,SUCC,REG
    }

    private void buildLoginWindow(){
        lblNickname = new Label("Nickname");
        lblNickname.setMinWidth(100);
        lblNickname.setAlignment(Pos.CENTER_LEFT);
        nickname = new TextField();
        lblPassword = new Label("Passwort");
        password = new PasswordField();
        btnLog = new Button("Login");
        btnReg = new Button("Registrieren");
        btnRow = new HBox(btnLog, btnReg);
        btnRow.setSpacing(20);
        btnRow.setAlignment(Pos.CENTER);

        gp.add(lblNickname, 1, 1);
        gp.add(nickname, 2, 1);
        gp.add(lblPassword, 1, 2);
        gp.add(password, 2, 2);
        gp.add(btnRow,1,4,2,1);
        gp.setVgap(10);
        gp.setPadding(new Insets(20,20,20,20));
        gp.setAlignment(Pos.CENTER);
    }

    private void buildRegisterWindow(){
        lblNickname2 = new Label("Nickname");
        lblNickname2.setMinWidth(100);
        lblNickname2.setAlignment(Pos.CENTER_LEFT);
        nickname2 = new TextField();
        lblPassword2 = new Label("Passwort");
        password2 = new PasswordField();
        lblpasswordWdh = new Label("Passwort wiederholen");
        passwordWdh = new PasswordField();
        mailLabel = new Label("E-Mail Adresse");
        eMail = new TextField();
        btnLog2 = new Button("Login");
        btnReg2 = new Button("Registrieren");
        btnRow2 = new HBox(btnLog2, btnReg2);
        btnRow2.setSpacing(20);
        btnRow2.setAlignment(Pos.CENTER);


        gp2.add(lblNickname2, 1, 1);
        gp2.add(nickname2, 2, 1);
        gp2.add(lblPassword2, 1, 2);
        gp2.add(password2, 2, 2);
        gp2.add(lblpasswordWdh,1,3);
        gp2.add(passwordWdh,2,3);
        gp2.getChildren().remove(btnRow);
        gp2.add(mailLabel,1,4);
        gp2.add(eMail,2,4);
        gp2.add(btnRow2,1,6,2,1);
        gp2.setVgap(10);
        gp2.setPadding(new Insets(20,20,20,20));
        gp2.setAlignment(Pos.CENTER);
    }

    //Pop-Up für Fehler und Info-Benachrichtigungen bei Login- bzw. Registrierungsprozess
    public void popUpMsg(msgType type, String content) {
        Alert alert = null;
        String lbl = "";
        String cont = "";
        Alert.AlertType t = Alert.AlertType.INFORMATION;

        ButtonType buttonTypeOne= new ButtonType("OK");

        if(type==msgType.SUCC){
            t = Alert.AlertType.INFORMATION;
            lbl = "Login";
            cont = content;
        }else if (type == msgType.ERR){
            t = Alert.AlertType.ERROR;
            lbl = "Fehler bei der Anmeldung";
            cont = content;
        }else if (type == msgType.REG){
            t= Alert.AlertType.INFORMATION;
            lbl = "Registrierung";
            cont= content;
        }

        alert = new Alert(t);
        alert.setTitle(lbl);
        alert.setHeaderText(null);
        alert.setContentText(cont);

        alert.getButtonTypes().setAll(buttonTypeOne);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne && type==msgType.SUCC) {
            primaryStage.close();
            gui.openChatWindow();
            alert.close();
        } else {
            alert.close();
        }
    }
}
