package Client;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lib.Content.*;
import lib.Util.Serializer;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Client {
    private MqttClient mqttClient = null;

    private static ServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;
    private Session session;
    private ClientCallback clientCallback;
    private LocalStorageHandler localStorageHandler;
    private Queue<Message> msgQueue = new LinkedList<>();
    private Queue<ChatRequest> chatRequestQueue = new LinkedList<>();
    private User loggedIn;
    private String userRequestTopic;


    /**
     * Baut Client Objekt, Baut verbindung zu MQTT-Broker auf
     */
    public Client(User usr) {

        this.loggedIn = usr;
        try {
            // MQTT Verbindung aufbauen
            mqttClient = new MqttClient("tcp://chat.dobisch.online:32768", this.getClientID(), new MemoryPersistence());
            clientCallback = new ClientCallback(this, mqttClient);
            mqttClient.setCallback(clientCallback);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            mqttClient.connect(connOpts);
            localStorageHandler = new LocalStorageHandler();
            userRequestTopic = "RequestChat/" + usr.getNickName();
            subscribeOnTopic(userRequestTopic);
            System.out.println("Subscribed on: RequestChat/" + usr.getNickName());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Chat sendChatRequest(User usr,String name){
        ChatRequest req = new ChatRequest();
        Chat chat = null;
        try {
            chat = createChat(name);
            req.setChat(chat);
            mqttClient.publish("RequestChat/" + usr.getNickName(), req.toString().getBytes(), 2, false);
            System.out.println("Sent Chat Request to " + usr.getNickName());
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        getLocalStorageHandler().save(chat);
        enterChatRoom(chat);
        return chat;
    }

    private static SessionFactory configureSessionFactory() throws HibernateException {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Message.class).configure();
        configuration.addAnnotatedClass(User.class).configure();
        //configuration.addAnnotatedClass(Profile.class).configure();

        Properties properties = configuration.getProperties();

        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }

    /**
     * TODO: Entscheiden wie hier die Architektur aussieht!
     * Gibt die Eindeutige Client ID zurück. Die Client ID ist die eindeutige Kennung jedes Benutzers.
     * @return Die eindeutige Client Kennung.
     */
    public String getClientID() {
        // TODO Funktioniert noch nicht, da User ID aus DB nicht ausgelesen werden kann return loggedIn.get_id();

        //vorübergehende Lösung
        return "client" + System.currentTimeMillis();
    }

    /**
     * Wird Getriggert wenn Message-Objekt empfangen Wurde.
     * @param msg Empfangenes Message-Objekt.
     */
    public void addMsgToQueue(Message msg) {
        this.msgQueue.add(msg);
    }

    /**
     * @return gibt erstes Element in der Message Queue zurück und entfernt dieses anschließend
     */
    public Message getNextMessage() {
        if (!this.msgQueue.isEmpty()) {
            return this.msgQueue.remove();
        }
        return null;
    }

    public Boolean hasNextMessage() {
        return !msgQueue.isEmpty();
    }

    /**
     * Versendet ein Message-Objekt.
     * @param msg Zu versendendes Message-Objekt.
     */
    public void sendMessage(Message msg) {
        try {
            String msgtopic = "/Chatrooms/" + msg.getChatmessageid() + "/Receive";
            mqttClient.publish(msgtopic, Serializer.MessageToJson(msg).getBytes(), 2, false);
            System.out.println("Sent message on Topic: " + msgtopic + " : " + msg);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    /**
     * Subscribed anhand einer ChatMessageID
     * @param chat Eindeutige Chat Kennungsnummer
     * @return Ob erfolgreich subscribed wurde.
     */
    public boolean enterChatRoom(Chat chat) {
        String topic = "/Chatrooms/" + chat.getChatmessageid() + "/Send";
        System.out.println("Subscribe on: " + topic);
        return subscribeOnTopic(topic);
    }

    /**
     * Subscribed sich beim MQTT-Broker auf einen Kanal.
     * @param topic Der Kanal welcher Abboniert werden soll.
     * @return Ob erfolgreich subscribed wurde.
     */
    public boolean subscribeOnTopic(String topic) {
        try {
            mqttClient.subscribe(topic);
        } catch (MqttException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void stop() {
        try {
            mqttClient.disconnect();
            mqttClient.close();
            clientCallback.stop();
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    public List<Chat> getChats() {
        return localStorageHandler.getChats();
    }

    public Chat createChat(String Name) throws MalformedURLException {
        Chat chat = ServerUtils.getServerWS().createChat(Name);
        localStorageHandler.save(chat);
        return chat;

    }

    public LocalStorageHandler getLocalStorageHandler() {
        return localStorageHandler;
    }

    /**
     * Dient der Clientseitigen verschlüsselung, um Passwort an Server zu übertragen
     * @param str
     * @return Passwort-Hash
     */
    public static String sha256(String str) {
        String hex = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str.getBytes(StandardCharsets.UTF_8));
            byte[] digest = md.digest();
            hex = String.format("%064x", new BigInteger(1, digest));
        } catch (Exception e) {
            System.out.println("Error while hashing password!");
        }
        return hex;
    }

    /**
     * Login-Methode
     * @param username
     * @param password
     * @return kann gültiges Userobjekt (isError==null) oder Fehlerobjekt (Fehlermeldung in String "isError") enthalten
     */
    public static User login(String username, String password){
        User usr = null;
        try {
            usr = ServerUtils.getServerWS().login(username, sha256(password));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return usr;
    }

    /**
     * Wird verwendet, um User zu erstellen
     * @param username
     * @param password
     * @return Rückgabewert ist Fehlermeldung
     */
    public static String register(String username, String password){
        User usr;
        if(checkForSpecialCharacter(username))
            return "Fehler! Benutzername darf keine Sonderzeichen enthalten oder leer sein!";
        else if (checkForWhitespace(username))
            return "Fehler! Benutzername darf keine Leerzeichen enthalten";
        else if (checkForWhitespace(password) || password.equals(""))
            return "Fehler! Passwort darf keine Leerzeichen enthalten oder leer sein";
        else if (searchForUser(username).getErrormsg() == null)
            return "Fehler! Nickname bereits vergeben";
        else {
            try {
                usr = ServerUtils.getServerWS().createUser(username, sha256(password));
                return usr.getErrormsg();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return "Error Client.Register!";
    }

    /**
     * Überprüft String auf Leerzeichen
     * @param str
     * @return Falls Rückgabewert true, wurde mind. ein Leerzeichen gefunden
     */
    public static boolean checkForWhitespace(String str){
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }

    public static boolean checkForSpecialCharacter(String str){
        if (!str.matches("[a-zA-Z_0-9]+")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param username Nutzername als String, der gesucht werden soll
     * @return  Gültiger nutzer, fals Nutzer in der DB gefunden wurde, sonst Erroruser (nur Errormsg gesetzt)
     */
    public static User searchForUser(String username) {
        User usr = null;
        try {
            usr = ServerUtils.getServerWS().searchForUser(username);
            if (usr.getErrormsg() == null) {
                System.out.println("Der Benutzer " + username + " wurde gefunden!");
            } else {
                //nicht gefunden
                System.out.println(usr.getErrormsg());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return usr;
    }

    public User getLoggedInUser() {
        if(loggedIn!=null) return loggedIn;
        else return null;
    }

    public User getLoggedIn() {
        return loggedIn;
    }

    public void addChatRequestToQueue(ChatRequest chatRequest) {
        chatRequestQueue.add(chatRequest);
    }

    public Queue<ChatRequest> getChatRequestQueue() {
        return chatRequestQueue;
    }

    public void persistateUser() throws MalformedURLException {
        ServerUtils.getServerWS().saveUser(this.loggedIn);
    }
}
