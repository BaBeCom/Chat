package Client;

import javafx.scene.image.Image;
import javafx.stage.FileChooser;

import java.io.*;

public class LoadImage {

    Image image;

    public Image loadImage() {

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*JPG");
        FileChooser.ExtensionFilter extensionFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*PNG");
        fileChooser.getExtensionFilters().addAll(extensionFilterJPG, extensionFilterPNG);

        File file = fileChooser.showOpenDialog(null);
        String path = file.getAbsolutePath();
        System.out.println(path);

        try {
            InputStream inputStream = new FileInputStream(path);
            image = new Image(inputStream, 80,80,false,false);
        } catch (IOException ex) {
            ex.getMessage();
        }
        return image;
    }
}
