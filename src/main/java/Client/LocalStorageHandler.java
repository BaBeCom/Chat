package Client;

import lib.Content.Chat;
import lib.Content.Message;
import lib.Content.Profile;
import lib.Content.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.Query;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class LocalStorageHandler implements CacheLoader {
    private static ServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;

    /**
     * Hibernate Initialisieren
     */
    public LocalStorageHandler() {
        if(serviceRegistry==null || sessionFactory==null)
            configureSessionFactory();
    }

    private static SessionFactory configureSessionFactory() throws HibernateException {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Message.class).configure();
        configuration.addAnnotatedClass(User.class).configure();
        configuration.addAnnotatedClass(Chat.class).configure();
        configuration.addAnnotatedClass(Profile.class).configure();

        Properties properties = configuration.getProperties();

        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }

    public void save(Message msg) {
        saveObject(msg);
    }

    public void save(Chat chat) {
        saveObject(chat);
    }

    private void saveObject(Object object) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(object);
        tx.commit();
        session.close();
    }

    private void saveObjects(Collection<Message> objects) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        for (Object object :
                objects) {
            session.save(object);
        }
        tx.commit();
        session.close();
    }


    @Override
    public Collection<Message> getMessagesByChatID(Long ChatID) {
        Session session = sessionFactory.openSession();
        String sql = "from lib.Content.Message as m where m.chatmessageid = :chatmessageid order by m.numInChat asc ";
        Query messageQuery = session.createQuery(sql);
        messageQuery.setParameter("chatmessageid", ChatID);
        return messageQuery.getResultList();
    }
    @Override
    public Collection<Message> getMessagesByChatIDUntil(Long ChatID,long UntilID) {
        Session session = sessionFactory.openSession();
        String sql = "from lib.Content.Message as m where m.chatmessageid = :chatmessageid AND m.chatmessageid < :UntilID order by m.numInChat asc ";
        Query messageQuery = session.createQuery(sql);
        messageQuery.setParameter("chatmessageid", ChatID);
        messageQuery.setParameter("UntilID", UntilID);
        return messageQuery.getResultList();
    }

    public void stop() {
    }

    @Override
    public List<Chat> getChats() {
        Session session = sessionFactory.openSession();
        List<Chat> chats = session.createNativeQuery("Select * from Chat").addEntity(Chat.class).list();
        session.close();
        return chats;
    }

    @Override
    public void updadateChat(Long chatMessageid, Long numInChat) {
        try {
            Message[] messages = ServerUtils.getMessagesFrom(chatMessageid, numInChat);
            saveObjects(Arrays.asList(messages));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Long maxNumInChat(Long chatMessageID) throws MalformedURLException {
        return ServerUtils.getServerWS().getMaxNumInChat(chatMessageID);
    }
}
