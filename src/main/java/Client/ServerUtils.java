package Client;

import Server.Server;
import lib.Content.Message;
import lib.Util.ServerWS;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Stellt Methoden bereit für direkte Server Kommunikation.
 * Wird für Administration der Kontakte, Chaträume, usw. benötigt.
 */
public class ServerUtils {
    private static ServerWS sws = null;

    /**
     * Gibt die aktuelle WebServer-verbindung. Falls nicht vorhanden wird eine erstellt.
     *
     * @return die aktuelle WebServer-Verbindung
     * @throws MalformedURLException Wenn Url nicht korrekt oder erreichbar ist
     */
    public static ServerWS getServerWS() throws MalformedURLException{
        if(sws == null) {
            URL url = new URL("http://" + Server.serverIP + ":6666/ws/serverws?wsdl");
            QName qname = new QName("http://Server/", "ServerWSImplService");
            Service service = Service.create(url, qname);
            sws = service.getPort(ServerWS.class);
        }
        return sws;
    }


    public static Message[] getMessagesFrom(Long chatMessageid, Long numInChat) {
        try {
            return getServerWS().getMessagesFrom(chatMessageid,numInChat);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new Message[0];
    }
}
