package Client;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import lib.Content.ChatRequest;
import lib.Content.Message;
import lib.Util.Serializer;
import org.eclipse.paho.client.mqttv3.*;

import java.io.File;

public class ClientCallback implements MqttCallback {

    private Client client;
    private MqttClient mqttClient;
    private LocalStorageHandler localStorageHandler;

    public ClientCallback(Client client, MqttClient mqttClient) {
        this.client = client;
        this.mqttClient = mqttClient;
        this.localStorageHandler = new LocalStorageHandler();
    }


    @Override
    public void connectionLost(Throwable throwable) {
        System.out.println("Connection Lost, Try Reconnect");
        boolean failed = false;
        try {
            mqttClient.reconnect();
        } catch (MqttException e) {
            failed = true;
            e.printStackTrace();
        }
        System.err.println("Reconnect " + (failed ? "indicates Success" : "Failed"));
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) {
        String message = mqttMessage.toString();
        System.out.println("Empfangen ( Topic: " + topic + ", Message: " + message.length() + " )");
        if (topic.matches("/Chatrooms/[a-zA-Z0-9]+/Send")) {
            try {
                Message msg = Serializer.MessageFromJson(message);
                System.out.println("Message Arrived on: " + topic + " : " + msg);
                localStorageHandler.save(msg);
                System.out.println("Saved Local");
                client.addMsgToQueue(msg);
                //Nachricht an GUI senden
                System.out.println("Put Message in Queue: " + msg.getText());
                return;
            } catch (ClassCastException e) {
                System.err.println("Fehler beim Class Cast!");
                e.printStackTrace();
            }
        } else if (topic.matches("RequestChat/[a-zA-Z0-9]+")) {
            try {
                ChatRequest chatRequest = Serializer.ChatRequestFromJson(message);
                System.out.println("ChatRequest Arrived on: " + topic + " : " + chatRequest.getChat().getChatmessageid());
                localStorageHandler.save(chatRequest.getChat());
                System.out.println("Saved Local");
                client.addChatRequestToQueue(chatRequest);
                //Nachricht an GUI senden
            } catch (ClassCastException e) {
                System.err.println("Fehler beim Class Cast!");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public void stop() {
        localStorageHandler.stop();
    }
}
