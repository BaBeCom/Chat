package Client;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import lib.Content.Chat;
import lib.Content.Message;

import java.io.File;

public class ConversationView extends VBox {
    private ObservableList speechBubbles = FXCollections.observableArrayList();
    private ScrollPane messageScroller;
    private VBox messageContainer;
    private HBox inputContainer;
    private BorderPane bp;
    private CacheLoader cacheLoader;

    public Long getChatmessageid() {
        return chat.getChatmessageid();
    }

    private Chat chat;
    private final Client client;

    //TODO: (Constructor parameter?) Conversation Partner
    public ConversationView(BorderPane borderPane, CacheLoader cacheLoader, Chat chat, Client client) {
        this.client = client;
        this.bp = borderPane;
        setupElements();
        this.cacheLoader = cacheLoader;
        this.chat = chat;
        loadMessageCache();
        client.enterChatRoom(chat);
    }

    private void loadMessageCache() {
        Long lastNumInChat = 0l;
        for (Message msg : cacheLoader.getMessagesByChatID(chat.getChatmessageid())) {
            addMessage(msg);
            lastNumInChat = msg.getNumInChat();
        }
        cacheLoader.updadateChat(getChatmessageid(), lastNumInChat);
        System.out.println("Loaded Cache for ChatMessageID: " + chat.getChatmessageid());
    }

    private void setupElements() {
        setupMessageDisplay();
        getChildren().setAll(messageScroller);
        setPadding(new Insets(5));
    }

    private void setupMessageDisplay() {
        messageContainer = new VBox(5);
        /**
         * Bindet Liste der Nachrichten (speechBubbles) an den messagesContainers, sodass diese synchronisiert sind
         */
        Bindings.bindContentBidirectional(speechBubbles, messageContainer.getChildren());

        messageScroller = new ScrollPane(messageContainer);
        messageScroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        messageScroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        messageScroller.setPrefHeight(700);
        messageScroller.prefWidthProperty().bind(messageContainer.prefWidthProperty().subtract(5));
        messageScroller.setFitToWidth(true);
        messageScroller.setFitToHeight(true);


        /**
         * Listener wird aktiv, wenn neue Nachricht der ObservableList speechBubbles hinzugefügt wird
         * updated den messageScroller
         */
        speechBubbles.addListener((ListChangeListener) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    messageScroller.setVvalue(messageScroller.getVmax());

                }
            }
        });
    }

    /**
     * @param msg Message, die der ObservableList speechBubble hinzugefügt wird
     *            Jedes Mal, wenn eine Nachricht der ObservableList hinzugefügt wird, erfolgt ein Update des messageScrollers
     *            Die SpeechDirection soll abhängig vom Verfasser angezeigt werden.
     */
    public void addMessage(Message msg) {
        if (msg.getAuthorNickName() != null && msg.getAuthorNickName().equals(this.client.getLoggedIn().getNickName())) {
            speechBubbles.add(new MessageBox(msg, SpeechDirection.RIGHT));
        } else {
            speechBubbles.add(new MessageBox(msg, SpeechDirection.LEFT));
            playSound();
        }
    }

    /**
     * wird verwendet, um Sound bei Empfang einer Nachricht abzuspielen.
     * Pfad zum Soundfile muss individuell angepasst werden.
     */
    private void playSound(){
        String musicFile = "C:\\Users\\tkafka\\OneDrive - bwedu\\Documents\\HSU\\INF5\\Software_Projekt\\chat\\src\\main\\resources\\sounds\\MetalPanelSuspended_Br.mp3";     // For example
        File f = new File(musicFile);
        System.out.println(f.exists());

        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
    }
}
