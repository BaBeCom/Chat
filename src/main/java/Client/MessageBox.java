package Client;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import lib.Content.Message;

enum SpeechDirection{
        LEFT, RIGHT
}

public class MessageBox extends HBox {
    private Message msg;
    private SpeechDirection direction;
    private Label displayedText;
    private Label timestamp;
    private ImageView imageView;
    private HBox container;
    private VBox msgComps;

    public MessageBox(Message msg, SpeechDirection direction){
        this.msg = msg;
        this.direction = direction;
        setupElements();
    }
    private void setupElements(){
        if (msg.getBytes() == null) {
            displayedText = new Label(msg.getText());
            displayedText.setPadding(new Insets(5,5,5,5));
            displayedText.setWrapText(true);
        } else {
            Image image = msg.byteToImage();
            imageView = new ImageView(image);
        }

        if(msg.getDate()==null){
            timestamp = new Label("unknown");
        }else{
            timestamp = new Label(msg.getDate().toString());
        }
        timestamp.setId("date");
        timestamp.setPadding(new Insets(5));
        timestamp.setWrapText(false);
        timestamp.setTextFill(Color.GRAY);
        if(msg.getBytes() == null)
            msgComps = new VBox(displayedText, timestamp);
        else
            msgComps = new VBox(imageView, timestamp);
        configure();
    }
    private void configure(){

        if(msg.getBytes() == null) {
            displayedText.setAlignment(Pos.CENTER_LEFT);
            container = new HBox(msgComps);
        }
        else {
            container = new HBox(imageView, timestamp);
        }
        if(direction == SpeechDirection.RIGHT) {
            container.setBackground(new Background(new BackgroundFill(Color.rgb(6, 212, 250), new CornerRadii(5, 0, 5, 5, false), Insets.EMPTY)));
            container.maxWidthProperty().bind(widthProperty().multiply(0.7));
            getChildren().setAll(container);
            setAlignment(Pos.CENTER_RIGHT);
        } else {
            container.setBackground(new Background(new BackgroundFill(Color.rgb(1, 253, 220), new CornerRadii(0, 5, 5, 5, false), Insets.EMPTY)));
            container.maxWidthProperty().bind(widthProperty().multiply(0.7));
            getChildren().setAll(container);
            setAlignment(Pos.CENTER_LEFT);
        }
    }
}
