package Client;

import Server.Server;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lib.Content.Profile;
import lib.Content.User;

import java.io.File;
import java.net.MalformedURLException;

public class ProfileView {

    Button btnSave;
    Button btnCancel;
    GUIfx gui;

    public ProfileView(GUIfx gui) {
        this.gui = gui;
    }

    public void run() {
        Stage stage = new Stage();
            start(stage);
    }

    public void start(Stage primaryStage) {

        primaryStage.setResizable(false);
        primaryStage.setTitle("Profile");
        primaryStage.getIcons().add(new Image("/img/babecom_icon.png"));
        GridPane gp = new GridPane();

        Label lblOldPassword = new Label("Altes Passwort");
        lblOldPassword.setMinWidth(100);
        lblOldPassword.setAlignment(Pos.CENTER_LEFT);
        PasswordField oldPassword = new PasswordField();

        Label lblNewPassword = new Label("Neues Passwort");
        lblNewPassword.setMinWidth(100);
        lblNewPassword.setAlignment(Pos.CENTER_LEFT);
        PasswordField newPassword = new PasswordField();

        Label lblNewPasswordRepetition = new Label("Wiederholung Passwort");
        lblNewPasswordRepetition.setMinWidth(100);
        lblNewPasswordRepetition.setAlignment(Pos.CENTER_LEFT);
        PasswordField newPasswordRepetition = new PasswordField();

        Label lblAvatar = new Label("Avatar");
        Button avatar = new Button("Upload Avatar");

        Label lblInterests = new Label("Info");
        TextField textFieldInterests = new TextField();

        btnSave = new Button("Speichern");
        btnCancel = new Button("Abbrechen");
        HBox btnRow = new HBox(btnSave, btnCancel);
        btnRow.setSpacing(20);
        btnRow.setAlignment(Pos.CENTER);

        avatar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gui.setImageView(new LoadImage().loadImage());
            }
        });

        //routine für Speichern
        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                /**
                 * Ist das Info-Feld nicht Leer, wird dessen Inhalt als Info in der DB gespeichert
                 */
                if(!textFieldInterests.getText().isEmpty()){
                    gui.getClient().getLoggedIn().setInfo(textFieldInterests.getText());
                    try {
                        ServerUtils.getServerWS().saveUser(gui.getClient().getLoggedIn());
                    } catch (MalformedURLException e){
                        e.printStackTrace();
                    }
                }

                /**
                 * Wenn mindestens eines der Passwort-Felder nicht leer ist, wird der Inhalt aller überprüft und ggf. eine Passwortänderung
                 * in der Datenbank durchgeführt
                 */
                if(!oldPassword.getText().isEmpty() && newPassword.getText().isEmpty() && newPasswordRepetition.getText().isEmpty()) {
                    if (Client.checkForWhitespace(newPassword.getText())) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "Passwort darf keine Leerzeichen enthalten!", ButtonType.CLOSE);
                        a.showAndWait();
                    } else if (!newPassword.getText().equals(newPasswordRepetition)) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "Passwort stimmt nicht mit Wiederholung überein!", ButtonType.CLOSE);
                        a.showAndWait();
                    } else if (Client.sha256(oldPassword.getText()).equals(Client.searchForUser(gui.getClient().getLoggedInUser().getNickName()).getPassword())) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "Altes Passwort ist nicht korrekt!", ButtonType.CLOSE);
                        a.showAndWait();
                    } else {
                        gui.getClient().getLoggedIn().setPassword(Client.sha256(newPassword.getText()));
                        try {
                            ServerUtils.getServerWS().saveUser(gui.getClient().getLoggedIn());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        oldPassword.clear();
                        newPassword.clear();
                        newPasswordRepetition.clear();
                    }
                }
            }
        });

        //routine für Abbruch
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gui.setProfileWindowOpen(false);
                primaryStage.close();
            }
        });

        gp.add(lblOldPassword,1,1);
        gp.add(oldPassword,2,1);
        gp.add(lblNewPassword,1,2);
        gp.add(newPassword,2,2);
        gp.add(lblNewPasswordRepetition,1,3);
        gp.add(newPasswordRepetition,2,3);
        gp.add(lblAvatar, 1, 4);
        gp.add(avatar, 2, 4);
        gp.add(lblInterests, 1, 5);
        gp.add(textFieldInterests, 2, 5);
        gp.add(btnRow,1,7,2,1);
        gp.setVgap(10);
        gp.setPadding(new Insets(20,20,20,20));
        gp.setAlignment(Pos.CENTER);

        Scene sc = new Scene(gp);
        sc.getStylesheets().add(GUIfx.class.getResource(
                "/css/statusBorderBackground.css").toExternalForm());

        //wird ausgeführt, wenn Login-Fenster geschlossen wird
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                gui.setProfileWindowOpen(false);
                primaryStage.close();
            }
        });

        primaryStage.setScene(sc);
        primaryStage.show();
    }
}
