package Client;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import lib.Content.User;
import lib.Util.GeneralUtils;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

public class ChatroomView extends ListView {
    private ObservableList<User> contacts = FXCollections.observableArrayList();
    private GUIfx gui;
    private String selectedView;
    private ConversationView conversationView;
    HashMap<String, ConversationView> conversationViewContainer;

    public ChatroomView(GUIfx guifx) {
        gui = guifx;
        conversationViewContainer = new HashMap<String, ConversationView>();
        update();
        this.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        /**
         * Behandelt angeklickte Nutzer in der Kontaktliste
         */
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                User usr = (User) getSelectionModel().getSelectedItem();
                System.out.println(usr.getNickName());
                User selectedItem = (User) getSelectionModel().getSelectedItem();
                if (selectedItem == null)
                    return;
                // Nutzer ist in Liste
                if (conversationViewContainer.containsKey(selectedItem.getNickName())) {
                    System.out.println("Nutzer ist in Liste gefunden Worden");
                    conversationView = conversationViewContainer.get(selectedItem.getNickName());
                    gui.updateCV(conversationView);
                    selectedView = selectedItem.getNickName();
                }
                // Nutzer ist nicht in Liste
                else {
                    System.out.println("Nutzer ist nicht in Liste gefunden worden");
                    try {
                        conversationView = new ConversationView(gui.getBorderPane(),
                                gui.getClient().getLocalStorageHandler(),
                                gui.getClient().createChat(GeneralUtils.createChatName(gui.getClient().getLoggedInUser(), selectedItem))
                                , gui.getClient());
                        selectedView = selectedItem.getNickName();
                        conversationViewContainer.put(selectedItem.getNickName(), conversationView);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    gui.updateCV(conversationView);

                }
                gui.updateConversationStatus(selectedItem);
            }
        });
    }

    private ObservableList<User> convertToObservableList(User[] usrArr) {
        ObservableList<User> obsList = FXCollections.observableArrayList();
        for (int i = 0; i < usrArr.length; i++) {
            if (usrArr[i] == null) {
                return obsList;
            }
            obsList.add(usrArr[i]);
        }
        return obsList;
    }

    public void update() {
        //TODO Persistente speichern von Usern in Kontaktliste
        this.setItems(convertToObservableList(gui.getClient().getLoggedInUser().returnContactList()));
    }

    public ConversationView getSelectedChat() {
        return conversationView;
    }

    public Map<String, ConversationView> getConversationViewContainer() {
        return conversationViewContainer;
    }
}
