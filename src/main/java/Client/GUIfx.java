package Client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import lib.Content.Chat;
import javafx.stage.WindowEvent;
import lib.Content.Message;
import lib.Content.User;
import org.apache.commons.lang.StringUtils;

import static java.lang.Thread.sleep;

public class GUIfx extends Application {

    private Image imgProfile;
    private ImageView imageViewProfile;
    private HBox borderImageViewProfile;
    private Client client;
    private Label label;
    private TextArea taSendmsg;
    private Button btnSend;
    private Button btnSendImage;
    private BorderPane borderPane;
    private ConversationView cv;
    private HBox sendingView;
    private ChatroomView chatroomView;
    private HBox headerView;
    private TextField addUserBox;
    private Button addUserButton;
    private LoginView loginView;
    private Stage primaryStage;
    private VBox leftView;
    private ComboBoxStatus comboBoxStatus;
    private Label conversationStatus;
    private boolean profileWindowOpen;

    public static void main(String[] args) {
        launch(args);
    }

    public void init() {
        System.setProperty("glass.accessible.force", "false"); //Einstellung um Bug in JDK im Zusammenhang mit ComboBox vorzubeugen
        label = new Label("BaBeCom");
        taSendmsg = new TextArea();
        taSendmsg.setPromptText("Enter Message...");
        btnSend = new Button("Send");
        btnSendImage = new Button("Send Image");
        borderPane = new BorderPane();

        sendingView = addSendingView();
        addUserBox = new TextField();
        addUserBox.setPromptText("Enter Nickname...");
        addUserButton = new Button("Add User");
        conversationStatus = new Label();
        conversationStatus.setFont(Font.font(24));
        conversationStatus.setPadding(new Insets(70,0,0,80));



        imgProfile = new Image("/img/icon2.jpg", 80, 80, true, true);
        imageViewProfile = new ImageView(imgProfile);
        imageViewProfile.setPreserveRatio(true);
        imageViewProfile.setSmooth(true);
        borderImageViewProfile = new HBox();
        String style_inner = "-fx-border-color: green;"
                + "-fx-border-width: 2;"
                + "-fx-border-style: solid;";
        borderImageViewProfile.setStyle(style_inner);
        borderImageViewProfile.getChildren().add(imageViewProfile);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.getIcons().add(new Image("/img/babecom_icon.png"));
        this.loginViewInitialization();

        //Nachrichten versenden
        HBox hBoxBottom = addSendingView();
        this.primaryStage = primaryStage;
        primaryStage.setMaximized(true);

        hBoxBottom.getChildren().addAll(taSendmsg, btnSend, btnSendImage);
        borderPane.setBottom(hBoxBottom);
        sendingView.getChildren().addAll(taSendmsg, btnSend, btnSendImage);
        borderPane.setBottom(sendingView);
        borderPane.setCenter(cv);

        Scene scene = new Scene(borderPane, 1200, 800);

        scene.getStylesheets().add(GUIfx.class.getResource(
                "/css/statusBorderBackground.css").toExternalForm());

        taSendmsg.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    buildMessage();
                }
            }
        });

        /**
         * Button Listener, um Nachrichten aus der Textbox abzusenden
         */
        btnSend.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                buildMessage();
            }
        });

        /**
         * Button Listener, um Bilder auszuwählen und abzusenden
         */
        btnSendImage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Message imageMsg = new Message();
                imageMsg.setChatmessageid(chatroomView.getSelectedChat().getChatmessageid());
                imageMsg.setAuthorNickName(client.getLoggedInUser().getNickName());
                imageMsg.setBytes(imageMsg.imageToByte(new LoadImage().loadImage()));
                client.sendMessage(imageMsg);
            }
        });


        /**
         * Listener wenn auf ProfilView geklickt wird
         */
        imageViewProfile.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (!profileWindowOpen) {
                profileWindowOpen = true;
                new ProfileView(this).start(new Stage());
                event.consume();
            }
        });

        addUserButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!StringUtils.isEmpty(addUserBox.getText())) {
                    User usr = client.searchForUser(addUserBox.getText());
                    if (usr.getErrormsg() == null) {
                        if (!usr.equals(client.getLoggedInUser())) {
                            if (client.getLoggedInUser().isInContactList(usr)) {
                                dialog("User " + usr.getNickName() + " is already in contact list", Alert.AlertType.ERROR);
                            } else {
                                client.getLoggedInUser().addContactToContactList(usr);
                                //dialog("User " + usr.getNickName() + " added successfully to contact list.", Alert.AlertType.INFORMATION);
                                chatroomView.update();
                            }
                        } else {
                            dialog("Can not add yourself to contact list. Please enter another nickname.", Alert.AlertType.ERROR);
                        }
                    } else {
                        dialog("No User with such nickname found.", Alert.AlertType.INFORMATION);
                    }
                } else {
                    dialog("Please enter nickname!", Alert.AlertType.ERROR);
                }
            }
        });

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(1);
            }
        });

        primaryStage.setTitle("BaBeCom");
        borderPane.getChildren().addAll(label); //nur Elemente adden, was nicht schon mit borderPane.set...() geadded wurde
        primaryStage.setScene(scene);
    }

    private void buildMessage() {
        if (!taSendmsg.getText().isEmpty() && !taSendmsg.getText().equals("\n")) {
            Message msg = new Message();
            msg.setChatmessageid(chatroomView.getSelectedChat().getChatmessageid());
            msg.setText(taSendmsg.getText());
            msg.setAuthorNickName(client.getLoggedInUser().getNickName());
            client.sendMessage(msg);
            taSendmsg.clear();
            taSendmsg.setCursor(Cursor.DEFAULT);
        }
    }

    private void dialog(String info, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText(info);
        alert.showAndWait();
    }


    private VBox addleftView() {
        HBox searchWrapper = new HBox(addUserBox, addUserButton);
        searchWrapper.setHgrow(addUserBox, Priority.ALWAYS);
        VBox vBoxLeft = new VBox(searchWrapper, chatroomView);
        vBoxLeft.setPadding(new Insets(5, 5, 5, 5));
        vBoxLeft.setSpacing(10);
        vBoxLeft.setVgrow(chatroomView, Priority.ALWAYS);
        return vBoxLeft;
    }

    private HBox addHeaderView() {
        HBox hBoxTop = new HBox();
        hBoxTop.setPadding(new Insets(5, 5, 5, 5));
        hBoxTop.setSpacing(10);
        comboBoxStatus = new ComboBoxStatus(this, borderImageViewProfile);
        return hBoxTop;
    }

    private HBox addSendingView() {
        HBox hBoxBottom = new HBox();
        hBoxBottom.setPadding(new Insets(5, 5, 5, 5));
        hBoxBottom.setSpacing(10);
        //Sorgt dafür, dass sich die Message TextArea auf die volle Breite in der HBox erstreckt.
        hBoxBottom.setHgrow(taSendmsg, Priority.ALWAYS);
        return hBoxBottom;
    }

    private void clientInitialization(User usr) {
        this.client = new Client(usr);

        /**
         * Thread holt im Abstand von x Sekunden Nachrichten beim Client ab, wenn neue Verfügbar sind.
         */
        Task messageUpdate = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while (true) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            while (!client.getChatRequestQueue().isEmpty()) {
                                Chat chat = client.getChatRequestQueue().remove().getChat();
                                client.subscribeOnTopic(chat.getChatmessageid().toString());
                            }
                            while (client.hasNextMessage()) {
                                Message msg = client.getNextMessage();
                                System.out.println("Message in Queue: " + msg);
                                if (msg != null) {
                                    for (String a :
                                            chatroomView.getConversationViewContainer().keySet()) {
                                        if (msg.getChatmessageid().equals(chatroomView.getConversationViewContainer().get(a).getChatmessageid()))
                                            chatroomView.getConversationViewContainer().get(a).addMessage(msg);
                                    }
                                    System.out.println("GUI Message update: " + msg.getText());
                                } else {
                                    // System.out.println("No new Messages.");
                                }
                            }
                        }
                    });
                    sleep(1000);
                }
            }
        };
        Thread msgUpdate = new Thread(messageUpdate);
        msgUpdate.setDaemon(true);
        msgUpdate.start();
    }

    /**
     * Startet Login-Fenster
     */
    private void loginViewInitialization() {
        try {
            loginView = new LoginView(this);
            loginView.start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode wird von Loginview aufgerufen, nachdem Nutzer angemeldet wurde.
     * Enthält alle Schritte, die einen existierenden Clienten voraussetzen, inklusive Clienterstellung
     *
     * @param usr LoginView übergibt angemeldeten Nutzer zur Erstellung des Clienten
     */
    public void startClientInit(User usr) {
        this.clientInitialization(usr);
    }

    /**
     * wird von LoginView verwendet, um Chat erst zu öffnen wenn Login erfolgreich war.
     */
    public void openChatWindow() {
        chatroomView = new ChatroomView(this);
        leftView = addleftView();
        borderPane.setLeft(leftView);
        headerView = addHeaderView();
        headerView.getChildren().addAll(borderImageViewProfile, comboBoxStatus, conversationStatus);
        borderPane.setTop(headerView);
        primaryStage.show();
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

    @Override
    public void stop() {
        client.stop();
    }

    public Client getClient() {
        return client;
    }

    public void setProfileWindowOpen(boolean profileWindowOpen) {
        this.profileWindowOpen = profileWindowOpen;
    }

    public void updateCV(ConversationView cv) {
        this.cv = cv;
        this.borderPane.setCenter(cv);
    }

    public void setImageView(Image image) {
        if (imageViewProfile != null)
            imageViewProfile.setImage(image);
    }
    public void updateConversationStatus(User usr){
        if(usr.getStatus() == null){
            usr.setStatus("Offline");
        }
        conversationStatus.setText(usr.getNickName() + " | " + usr.getStatus() + "\nInfo: " + usr.getInfo());

    }
}
